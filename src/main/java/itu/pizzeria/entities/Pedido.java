package itu.pizzeria.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import itu.pizzeria.enums.EstadoPago;
import itu.pizzeria.enums.EstadoPedido;

@Entity
public class Pedido implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	@Enumerated(EnumType.STRING)
	private EstadoPedido estadoPedido;
	
	@Enumerated(EnumType.STRING)
	private EstadoPago estadoPago;

	@ManyToOne
	private Usuario cliente;

	@ManyToOne
	private Pizza pizza;

	private Boolean tipoEntrega;

	private Double precioTotal;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	public Pedido() {

	}

	public Pedido(String id, Usuario cliente, Pizza pizza, Boolean tipoEntrega, Double precioTotal, Date creado,
			Date editado, Date eliminado) {
		super();
		this.id = id;
		this.cliente = cliente;
		this.pizza = pizza;
		this.tipoEntrega = tipoEntrega;
		this.precioTotal = precioTotal;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Usuario getCliente() {
		return cliente;
	}

	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}

	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Boolean getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(Boolean tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public Double getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(Double precioTotal) {
		this.precioTotal = precioTotal;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
