package itu.pizzeria.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String nombre;
	private String apellido;
	private String eMail;
	private String direccion;
	private String telefono;

	private String rol;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	public String getId() {
		return id;
	}

	public Usuario(String id, String nombre, String apellido, String eMail, String direccion, String telefono,
			String rol, Date creado, Date editado, Date eliminado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.eMail = eMail;
		this.direccion = direccion;
		this.telefono = telefono;
		this.rol = rol;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

}
