package itu.pizzeria.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import itu.pizzeria.enums.Tamano;

@Entity
public class Pizza implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@Enumerated(EnumType.STRING)
	private Tamano tamano;
	
	@OneToMany
	private TipoMasa tipoMasa;
	@OneToMany
	private Ingredientes ingredientes;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	public Pizza() {

	}

	public Pizza(String id, Tamano tamano, TipoMasa tipoMasa, Ingredientes ingredientes, Date creado,
			Date editado, Date eliminado) {
		super();
		this.id = id;
		this.tamano = tamano;
		this.tipoMasa = tipoMasa;
		this.ingredientes = ingredientes;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public Tamano getTamano() {
		return tamano;
	}

	public void setTamano(Tamano tamano) {
		this.tamano = tamano;
	}

	public TipoMasa getTipoMasa() {
		return tipoMasa;
	}

	public void setTipoMasa(TipoMasa tipoMasa) {
		this.tipoMasa = tipoMasa;
	}

	public Ingredientes getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(Ingredientes ingredientes) {
		this.ingredientes = ingredientes;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

}
