package itu.pizzeria.enums;

public enum Tamano {
	
	PEQUEÑA,
	MEDIANA,
	GRANDE,
	FAMILIAR

}
