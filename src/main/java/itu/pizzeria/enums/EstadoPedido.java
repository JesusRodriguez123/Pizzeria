package itu.pizzeria.enums;

public enum EstadoPedido {
	
	ENESPERA,
	ENPROCESO,
	LISTORETIRO,
	LISTOENVIO,
	ENVIADO;

}
