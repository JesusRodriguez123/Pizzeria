package itu.pizzeria.enums;

public enum EstadoPago {
	
	PENDIENTE,
	PAGADO;
}
