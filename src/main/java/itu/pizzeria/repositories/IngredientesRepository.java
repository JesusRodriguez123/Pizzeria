package itu.pizzeria.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import itu.pizzeria.entities.Ingredientes;

@Repository
public interface IngredientesRepository extends JpaRepository<Ingredientes, String>{
	
	@Query("SELECT i FROM Ingredientes i WHERE i.id = :id AND i.eliminado IS NULL")
	public Ingredientes buscarPorId(@Param("id") String id);
	
	@Query("SELECT i FROM Ingredientes i WHERE i.nombre = :nombre AND i.eliminado IS NULL")
	public Ingredientes buscarPorNombre(@Param("nombre") String nombre);

	@Query("SELECT i FROM Ingredientes i WHERE i.eliminado IS NULL")
	public List<Ingredientes> buscarTodos();
	
	@Query("SELECT i FROM Ingredientes i WHERE i.eliminado IS NULL")
	public Page<Ingredientes> buscarPaginado(Pageable paginable);
	
	@Query("SELECT i FROM Ingredientes i WHERE i.eliminado IS NULL AND i.nombre LIKE :q")
	public Page<Ingredientes> buscarPaginado(Pageable paginable, @Param("q") String q);
}
