package itu.pizzeria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import itu.pizzeria.entities.Ingredientes;
import itu.pizzeria.services.IngredientesService;

@Controller
@RequestMapping("/ingredientes")
public class IngredientesController {

	@Autowired
	private IngredientesService ingredientesService;
	
	@GetMapping("/lista")
	public ModelAndView listar(Pageable paginable, @RequestParam(required = false) String q) {
		ModelAndView mav = new ModelAndView("ingrendientes");
		
		Page<Ingredientes> page = null;
		
		if (q == null || q.isEmpty()) {
			page = ingredientesService.buscarTodos(paginable);
		}else {
			page = ingredientesService.buscarTodos(paginable, q);
			mav.addObject("q", q);
		}
		
		mav.addObject("page", page);
		
		return mav;
	}
	
//	@PostMapping
//	public ModelAndView guardar(@Valid @ModelAttribute("ingrediente") Ingredientes ingrediente, BindingResult result, ModelMap model) {
//	
//	
//	return ModelAndView("vista");
//	}
}
