package itu.pizzeria.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import itu.pizzeria.entities.Ingredientes;
import itu.pizzeria.repositories.IngredientesRepository;

@Service
public class IngredientesService {

	@Autowired
	private IngredientesRepository ingredientesRepository;

	public Ingredientes crear(String id, String nombre, Double precio) throws Exception {
		Ingredientes ingrediente = new Ingredientes();
		if (!id.isEmpty() || id != null) {
			ingrediente = ingredientesRepository.buscarPorId(id);
		}

		if (nombre == null || nombre.isEmpty()) {
			throw new Exception("El nombre del ingrediente no puede estar vacío.");
		}

		if (precio == 0 || precio.isNaN() || precio > 100) {
			throw new Exception("El precio no puede ser 0, ni ser mayor a 100 pesos");
		}

		ingrediente.setNombre(nombre);
		ingrediente.setPrecio(precio);

		return guardar(ingrediente);
	}

	public Ingredientes eliminar(String idIngrediente) {
		Ingredientes ingrediente = ingredientesRepository.buscarPorId(idIngrediente);
		return guardar(ingrediente);
	}

	@Transactional
	private Ingredientes guardar(Ingredientes ingrediente) {
		return ingredientesRepository.save(ingrediente);
	}

	public Page<Ingredientes> buscarTodos(Pageable paginable, String q) {
		return ingredientesRepository.buscarPaginado(paginable, q);
	}

	public Page<Ingredientes> buscarTodos(Pageable paginable) {
		return ingredientesRepository.buscarPaginado(paginable);
	}

	public Ingredientes buscarIngredientePorId(String idIngrediente) {
		return ingredientesRepository.buscarPorId(idIngrediente);
	}

	public Ingredientes buscarIngredientePorNombre(String nombre) {
		return ingredientesRepository.buscarPorNombre(nombre);
	}

}
